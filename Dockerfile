FROM ruby:2.7.2
RUN wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
RUN echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list
RUN apt-get update -qq && apt-get install -y gnupg nodejs imagemagick mongodb-org postgresql-client

RUN mkdir /locomotiveapp
WORKDIR /locomotiveapp
COPY Gemfile /locomotiveapp/Gemfile
COPY Gemfile.lock /locomotiveapp/Gemfile.lock
RUN bundle install
COPY . /locomotiveapp

# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000

# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]